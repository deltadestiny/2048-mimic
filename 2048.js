
// Called when the window loads
window.onload = function()
{
	/************************************************************************
		HEADER
	************************************************************************/
	var DIMENSION 	= 4;	// board dimension
	
	var UP			= 1;
	var DOWN		= 2;
	var LEFT		= 3;
	var RIGHT		= 4;
	
	var tile 		= 64;	// size of each tile
	var gridSize 	= 70;	// size of each grid
	
	// visualize the board as a 2D array
	//var board = [ [0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0] ];
	var board = [ [], [], [], [] ];
	var tileCount = 0;
	
	// initialize Phaser
	var game = new Phaser.Game( gridSize * DIMENSION,	// width
								gridSize * DIMENSION,	// height
								Phaser.Auto,			// renderer
								document.getElementById("gameContainer"),	// parent
								{ preload: Awake, create: Start }
								);
								
	// keyboard input
	var up;
	var down;
	var left;
	var right;
	
	// references to count how many of each has been produced
	// tVar holds temporary data when calculations are being done
	var seed		= 0,	tSeed		= 0;	// 2
	var water		= 0,	tWater		= 0;	// 4
	var sapling		= 0,	tSapling	= 0;	// 8
	var pot			= 0,	tPot		= 0;	// 16
	var biggerpot	= 0,	tBiggerpot	= 0;	// 32
	var tree		= 0,	tTree		= 0;	// 64
	var forest		= 0,	tForest		= 0;	// 128
	var city		= 0,	tCity		= 0;	// 256
	var people		= 0,	tPeople		= 0;	// 512
	var party		= 0,	tParty		= 0;	// 1024
	var world		= 0,	tWorld		= 0;	// 2048 - may not be used since ending the game requires only one
	
	// flag which allows swipe/movement
	var canMove = false;
	
	/************************************************************************
		FUNCTIONS
	************************************************************************/
	
	/*
		Initialize required items.
		Preload these images to be used as tiles.
		All icons listed here are made by Freepik from www.flaticon.com
	*/
	
	function Tile()
	{
		this.value = 0;
		this.sprite = null;
	}
	
	function Awake()
	{
		game.load.image("0",	"assets/e.png"		);
		game.load.image("2",	"assets/2.png"		);
		game.load.image("4",	"assets/4.png"		);
		game.load.image("8",	"assets/8.png"		);
		game.load.image("16",	"assets/16.png"		);
		game.load.image("32",	"assets/32.png"		);
		game.load.image("64",	"assets/64.png"		);
		game.load.image("128",	"assets/128.png"	);
		game.load.image("256",	"assets/256.png"	);
		game.load.image("512",	"assets/512.png"	);
		game.load.image("1024",	"assets/1024.png"	);
		game.load.image("2048",	"assets/2048.png"	);
		
		// add listeners to keys
		var up		= game.input.keyboard.addKey(Phaser.Keyboard.UP);
		var down	= game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		var left	= game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
		var right	= game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
		
		var space	= game.input.keyboard.addKey(Phaser.Keyboard.E);
		
		up.onDown.add( MoveBoardUp, this );
		down.onDown.add( MoveBoardDown, this );
		left.onDown.add( MoveBoardLeft, this );
		right.onDown.add( MoveBoardRight, this );
		
		space.onDown.add( PrintBoard, this );
	}
	
	// Begin to build the game
	function Start()
	{
		game.stage.backgroundColor = "FFFFFF"; //"#F0F8FF";
	
		ResetBoard();
		
		Insert(2);
		Insert(2);
	}
	
	// Inserts a number randomly on the board
	function Insert( number )
	{
		var tile;
		var tileX;
		var tileY;
		
		// if there are no empty spaces to insert new tile, return
		if ( !BoardHas( 0 ) )
			return false;
		
		do {
			// select a random empty tile(x,y) from the board
			tileX = Math.floor( Math.random() * DIMENSION );
			tileY = Math.floor( Math.random() * DIMENSION );
			
			tile = board[tileX][tileY];
		} while ( tile.value != 0 );
		
		// console.log( "Inserting at (" + (tileX+1) + "," + (tileY+1) + ")" );
		
		// place the number at the designated tile
		tile.value = number;
		
		// add the sprite using the Phaser framework
		/* 
			note: x & y are inverted here because board[row][column],
			but x refers to vertical height aka column,
			and y refers to horizontal position aka row.
		*/
		var sprite = game.add.sprite( tileY * gridSize, tileX * gridSize,
			GetSpriteName( number ) );
			
		// associate the tile's sprite with the Phaser sprite object
		// note: implementing a class isn't so bad of an idea too
		tile.sprite = sprite;
		
		// set the alpha to 0, tween it to appear in 500ms
		sprite.alpha = 0;
		
		// tween the newly created tile's alpha from 0 to 1
		var tween = game.add.tween(sprite);
		tween.to( { alpha: 1 }, 200 );
		tween.onComplete.add( function()
		{	
			if ( PlayerIsStuck() )
				EndTurn( true );
		
			canMove = true;
		} );
		tween.start();
		
		return true;
	}
	
	// Returns the sprite name as defined in Awake() when loading the tile images
	function GetSpriteName( number )
	{
		switch( number )
		{
			case 0: 	return "0";
			case 2: 	return "2";
			case 4: 	return "4";
			case 8: 	return "8";
			case 16:	return "16";
			case 32:	return "32";
			case 64:	return "64";
			case 128:	return "128";
			case 256:	return "256";
			case 512:	return "512";
			case 1024:	return "1024";
			case 2048:	return "2048";
			default: 	return "0";
		}
	}
	
	// Attempts to move a tile at (x,y) to the given direction.
	// Returns true if successful, false otherwise
	function MoveTile( x, y, direction )
	{
		// if this is an empty tile, ignore
		if ( board[x][y].value == 0 )
			return false;
		
		// if the tile is at the edge of the direction, ignore
		switch ( direction )
		{
			case UP: 	if ( x == 0 ) 				return false;	break;
			case DOWN: 	if ( x == DIMENSION - 1 ) 	return false;	break;
			case LEFT: 	if ( y == 0 ) 				return false;	break;
			case RIGHT:	if ( y == DIMENSION - 1 ) 	return false;	break;
			default: return false;
		}
		
		var newTile;				// refers to the top valid tile in the column
		var currTile = board[x][y];	// refers to this tile		
		var depth = 0;				// refers to the new position of the tile on the board (x or y)
		var tween;
		var hasMoved = false;		// did the tile move?
		
		var target = x;				// new position of the tile according to its moving axis
		
		switch ( direction )
		{
			case UP:
				// check the column for the rows above the tile for valid empty tiles or matching tiles
				for ( var row = 0; row < x; row++ )
				{
					if ( board[row][y].value == 0 || board[row][y].value == currTile.value )
					{
						// row is the furthest valid tile available
						if ( row < target ) target = row;
					}
					else target = x;
				}
				
				// if no valid tile is available, stop
				if ( target == x )
					return false;
				
				if ( board[target][y].value == 0 )
				{
					// empty tile found
					newTile = board[target][y];
					newTile.value = currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				else if ( board[target][y].value == currTile.value )
				{
					// matching tile found
					newTile = board[target][y];
					newTile.value += currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				break;
				
			case DOWN:
				// check each column for the rows below the tile for empty spaces or matching tiles
				for ( var row = DIMENSION - 1; row > x; row-- )
				{
					if ( board[row][y].value == 0 || board[row][y].value == currTile.value )
					{
						// row is the furthest valid tile available
						if ( row > target ) target = row;
					}
					else target = x;
				}
				
				// if no valid tile is available, stop
				if ( target == x )
					return false;
				
				if ( board[target][y].value == 0 )
				{
					// empty tile found
					newTile = board[target][y];
					newTile.value = currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				else if ( board[target][y].value == currTile.value )
				{
					// matching tile found
					newTile = board[target][y];
					newTile.value += currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				break;
				
			case LEFT:
				// check each row for the columns on the left for empty spaces or matching tiles
				for ( var col = 0; col < y; col++ )
				{
					if ( board[x][col].value == 0 || board[x][col].value == currTile.value )
					{
						// col is the furthest valid tile available
						if ( col < target ) target = col;
					}
					else target = y;
				}
				
				// if no valid tile is available, stop
				if ( target == y )
					return false;
				
				if ( board[x][target].value == 0 )
				{
					// empty tile found
					newTile = board[x][target];
					newTile.value = currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				else if ( board[x][target].value == currTile.value )
				{
					// matching tile found
					newTile = board[x][target];
					newTile.value += currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				break;
			
			case RIGHT:
				// check each row for the columns on the right for empty spaces or matching tiles
				for ( var col = DIMENSION - 1; col > y; col-- )
				{
					if ( board[x][col].value == 0 || board[x][col].value == currTile.value )
					{
						// col is the furthest valid tile available
						if ( col > target ) target = col;
					}
					else target = y;
				}
				
				// if no valid tile is available, stop
				if ( target == y )
					return false;
				
				if ( board[x][target].value == 0 )
				{
					// empty tile found
					newTile = board[x][target];
					newTile.value = currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				else if ( board[x][target].value == currTile.value )
				{
					// matching tile found
					newTile = board[x][target];
					newTile.value += currTile.value;
					currTile.value = 0;
					
					depth = target;
					hasMoved = true;
				}
				break;
				
			default: break;
		}
		
		// console.log("Distance: " + (x-depth));
		
		if ( hasMoved )
		{
			var duplicate = game.add.sprite(  y * gridSize, x * gridSize, currTile.sprite.key );
			tween = game.add.tween( duplicate );
			
			if ( direction === UP || direction === DOWN )
				tween.to( { y: gridSize * ( depth ) }, 100 );
			else if ( direction === LEFT || direction === RIGHT )
				tween.to( { x: gridSize * ( depth ) }, 100 );
			else
				return true;
				
			tween.onComplete.add( function()
			{
				duplicate.destroy();
				
				if ( direction === UP || direction === DOWN )
					UpdateSprite( newTile, depth, y );
				else if ( direction === LEFT || direction === RIGHT )
					UpdateSprite( newTile, x, depth );
			} );
			
			UpdateSprite( currTile, x, y );
			tween.start();
			
			return true;
		}
		else
			return false;
	}
	
	// Moves all the tiles in the board downwards
	function MoveBoardUp()
	{
		// if player cannot move, do nothing
		if ( !canMove )	return;
		else 			canMove = false;
			
		var movementPresent = false;
			
		for ( var row = 0; row < DIMENSION; row++ )
		{
			for ( var col = 0; col < DIMENSION; col++ )
			{
				if ( MoveTile( row, col, UP ) )
					movementPresent = true;
			}
		}
		
		EndTurn( movementPresent );
	}
	
	// Moves all the tiles in the board downwards
	function MoveBoardDown()
	{
		// if player cannot move, do nothing
		if ( !canMove )	return;
		else 			canMove = false;
		
		var movementPresent = false;
	
		for ( var row = DIMENSION - 1; row >= 0; row-- )
		{
			for ( var col = 0; col < DIMENSION; col++ )
			{
				if ( MoveTile( row, col, DOWN ) )
					movementPresent = true;
			}
		}
		
		EndTurn( movementPresent );
	}
	
	// Moves all the tiles in the board to the left
	function MoveBoardLeft()
	{
		// if player cannot move, do nothing
		if ( !canMove )	return;
		else 			canMove = false;
			
		var movementPresent = false;
			
		for ( var col = 0; col < DIMENSION; col++ )
		{
			for ( var row = 0; row < DIMENSION; row++ )
			{
				if ( MoveTile( row, col, LEFT ) )
					movementPresent = true;
			}
		}
		
		EndTurn( movementPresent );
	}
	
	// Moves all the tiles in the board to the right
	function MoveBoardRight()
	{
		// if player cannot move, do nothing
		if ( !canMove )	return;
		else 			canMove = false;
		
		var movementPresent = false;
	
		for ( var col = DIMENSION - 1; col >= 0; col-- )
		{
			for ( var row = 0; row < DIMENSION; row++ )
			{
				if ( MoveTile( row, col, RIGHT ) )
					movementPresent = true;
			}
		}
		
		EndTurn( movementPresent );
	}
	
	// Updates the sprite for the given tile
	function UpdateSprite( tile, x, y )
	{
		if ( tile.sprite == null )
		{
			//console.log( "[SPRITE]: Creating new sprite at ("+(x+1)+","+(y+1)+")" );
			tile.sprite = game.add.sprite( y * gridSize, x * gridSize,
				GetSpriteName( tile.value ) );
		}
		else
		{	
			//console.log( "[SPRITE]: Updating old sprite to " + GetSpriteName( tile.value ) );
			tile.sprite.key = GetSpriteName( tile.value );
			tile.sprite.loadTexture( tile.sprite.key );
			tile.sprite.x = y * gridSize;
			tile.sprite.y = x * gridSize;
		}
	}
	
	// Resets the data for the entire board
	function ResetBoard()
	{
		board = [ [], [], [], [] ];
		
		for ( var row = 0; row < DIMENSION; row++ )
		{
			for ( var col = 0; col < DIMENSION; col++ )
			{
				var newTile = new Tile();
				board[row][col] = newTile;
			}
		}
	}
	
	// Called at the end of a turn
	function EndTurn( hasMoved )
	{
		if ( hasMoved )
		{
			if ( BoardHas( 2048 ) )
			{
				// YOU WIN
				RecordBoardResult();
				UpdateResults();
				//document.getElementById( "unmasked" ).style.visibility = "visible";
			}
			else if ( Insert( 2 ) == false )
			{
				// GAME OVER
				RecordBoardResult();
				UpdateResults();
				//document.getElementById( "unmasked" ).style.visibility = "visible";
			}
			
			hasMoved = false;
			// console.log( "-" );
			// PrintBoard();
		}
		else
			canMove = true;
		
		// canMove is set to true in Insert()
	}
	
	// Checks the board if it has spaces left
	function BoardHas( value )
	{
		for ( var i = 0; i < DIMENSION; i++ )
			for ( var j = 0; j < DIMENSION; j++ )
				if ( board[i][j].value == value )
					return true;
					
		return false;
	}
	
	// Loops through each tile and adds the value of each tile to the score
	function RecordBoardResult()
	{
		for ( var i = 0; i < DIMENSION; i++ )
			for ( var j = 0; j < DIMENSION; j++ )
				if ( board[i][j].value != 0 )
					CalculateValueOf( board[i][j].value );
	}
	
	// Checks each tile for a same neighbour. If found, returns false. Otherwise, true.
	function PlayerIsStuck()
	{
		for ( var x = 0; x < DIMENSION; x++ )
		{
			for ( var y = 0; y < DIMENSION; y++ )
			{
				// If not top row, check the row above
				if ( x != 0 )
				{
					if ( board[x][y].value == board[x-1][y].value )
						return false;
				}
					
				// If not bottom row, check the row below
				if ( x != DIMENSION - 1 )
				{
					if ( board[x][y].value == board[x+1][y].value )
						return false;
				}
				
				// If not most left column, check the left column
				if ( y != 0 )
				{
					if ( board[x][y].value == board[x][y-1].value )
						return false;
				}
				
				// If not most right column, check the right column
				if ( y != DIMENSION - 1 )
				{
					if ( board[x][y].value == board[x][y+1].value )
						return false;
				}
			}
		}
		
		return true;
	}
	
	function CalculateValueOf( value )
	{	
		// add THIS value to the count
		switch ( value )
		{
			case 2048:	world		+= 1; break;
			case 1024:	party		+= 1; break;
			case 512:	people		+= 1; break;
			case 256:	city		+= 1; break;
			case 128:	forest		+= 1; break;
			case 64:	tree		+= 1; break;
			case 32:	biggerpot	+= 1; break;
			case 16:	pot			+= 1; break;
			case 8:		sapling		+= 1; break;
			case 4:		water		+= 1; break;
			case 2: 	seed		+= 1; break;
			default: break;
		}
		
		var accumulative = 1;
		
		// from THIS value, derive lower tile values
		// note: break is omitted so that entry into the code can be continued by flowing down
		switch ( value )
		{
			case 2048: 	party		+= accumulative * ( 2 ); accumulative *= 2;
			case 1024: 	people		+= accumulative * ( 2 ); accumulative *= 2;
			case 512:	city		+= accumulative * ( 2 ); accumulative *= 2;
			case 256: 	forest		+= accumulative * ( 2 ); accumulative *= 2;
			case 128: 	tree		+= accumulative * ( 2 ); accumulative *= 2;
			case 64: 	biggerpot	+= accumulative * ( 2 ); accumulative *= 2;
			case 32: 	pot			+= accumulative * ( 2 ); accumulative *= 2;
			case 16: 	sapling 	+= accumulative * ( 2 ); accumulative *= 2;
			case 8: 	water 		+= accumulative * ( 2 ); accumulative *= 2;
			case 4:		seed 		+= accumulative * ( 2 ); accumulative *= 2;
			default: break;
		}
	}
	
	// Updates the HTML page to display the result when end of game is reached
	function UpdateResults()
	{
		document.getElementById( "seed" 		).innerHTML = seed;
		document.getElementById( "water" 		).innerHTML = water;
		document.getElementById( "sapling" 		).innerHTML = sapling;
		document.getElementById( "pot" 			).innerHTML = pot;
		document.getElementById( "biggerpot" 	).innerHTML = biggerpot;
		document.getElementById( "tree" 		).innerHTML = tree;
		document.getElementById( "forest" 		).innerHTML = forest;
		document.getElementById( "city" 		).innerHTML = city;
		document.getElementById( "people" 		).innerHTML = people;
		document.getElementById( "party" 		).innerHTML = party;
		// 2048 is omitted because the count is not displayed on the HTML page
		
		document.getElementById( "resultsNote" ).style.display = "none";
		
		// only display the sections up to where the player reached
		if ( seed ) 		document.getElementById( "mSeed" ).style.display = "block";
		if ( water )		document.getElementById( "mWater" ).style.display = "block";
		if ( sapling )		document.getElementById( "mSapling" ).style.display = "block";
		if ( pot )			document.getElementById( "mPot" ).style.display = "block";
		if ( biggerpot )	document.getElementById( "mBiggerpot" ).style.display = "block";
		if ( tree)			document.getElementById( "mTree" ).style.display = "block";
		if ( forest)		document.getElementById( "mForest" ).style.display = "block";
		if ( city )			document.getElementById( "mCity" ).style.display = "block";
		if ( people)		document.getElementById( "mPeople" ).style.display = "block";
		if ( party )		document.getElementById( "mParty" ).style.display = "block";
		if ( world)			document.getElementById( "mWorld" ).style.display = "block";
		
		// display the 'Play' again button
		document.getElementById( "replay" ).style.display = "block";
	}
	
	function PrintBoard()
	{
		for ( var i = 0; i < DIMENSION; i++ )
		{
			console.log( "[" + board[i][0].value + "] " +
						"[" + board[i][1].value + "] " +
						"[" + board[i][2].value + "] " +
						"[" + board[i][3].value + "] " );
		}
	}
}